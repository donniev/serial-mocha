## 2015/10/31 DLV initial commit 0.0.1 unpublished
1. Initial commit

## 2015/10/31 DLV cleanup 0.0.1 unpublished
1. Cleaned up gulp to not copy gulpfile to dist directory
## 2015/11/01 DLV publish and tag 0.0.1 published
1. release initial version
## 2015/11/12 DLV publish and tag 0.0.2 published
1. Changed to use heirarchy for results
## 2015/11/12 DLV publish and tag 0.0.3 published
1. Cleanup and edit README

## 2015/11/27 DLV publish and tag 0.0.4 published
1. latest version of expect fails on global install


